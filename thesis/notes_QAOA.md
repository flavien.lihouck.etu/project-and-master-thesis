QAOA

# Résumé:

QAOA: combinatorial optim

$p$ higher better approximation

Tests on max-cut

## Intro

$n$ bits, $m$ clauses
$C$:Objective function is satisfying as many clauses

QC $2^{n}d$ Hilbert space (space defined by unit vector)
computational basis (?): $|z>$
Objective function == operator diagonal in the basis.

operator $U(C, \gamma) = \exp{-i\gamma C} = \prod_{\alpha = 1}^{m}{\exp{-i\gamma C_{\alpha}}}$

Commutativity because diagonal
Term locality == $\alpha$

Integral Eigen-values (?)-> $0 < \gamma < 2\pi$

$B$ sum of $\sigma^{x}$ -> single bit operators (spins of qbits)

$U(B, \beta) = \exp{-i\beta B} = \prod_{\j = 1}^{m}{\exp{-i\beta \sigma^{x}_{\alpha}}}$ with $0 <= \beta <= \pi$

Initial state $|s> = \frac{1}{\sqrt{2^{n}}}\sum_{z}|z>$

Quantum state defines as
$|\gamma, \beta> = U(B, \beta_{p})U(C, \gamma_{c}) ... U(B,\beta_{1}) U(C, \gamma_{1}) |s>$

This state can be obtained with depth $mp + p$

$F_{p}$ expectation of $C$

$M_{p} = \max_{\gamma, \beta} F_{p}(\gamma, \beta)$

Algo idea :
(1) fix a $p$
(2) select $(\gamma , \beta)$ making $F_{p} as large as possible (?).
(3) run the circuit to get state $|\gamma, \beta>$.

(4) measure in basis for $z$ (?)
(5) evaluate $C(z)$
(6) repeat so that $C(z) ~ or > F_{p}(\gamma, \beta)$

With $p !!= n$, grid search for best $(\gamma, \beta)$ on grid over $[0,2\pi]^{p} \times [0, \pi]^{p}$

-> Bounded by $O(m^{2} + mn)$

Run the state for optimized $(\gamma, \beta)$ -> $z$
Mean of $C(z) == M_{p}$

## Fixed p algo:

For a fixed p, classical preprocessing to find best $\gamma , \beta$.

Example with Maxcut:

qbits only interact in pairs for p = 1.

p fixes the maximum distance at which an edge is taken into account.

Estimation on classical computers indepedent of $n$

difficult stats.

## Concentration

For $v$ and $p$ fixed, distribution of $C(z)$ contracted near its mean.

-> probability and approx ratios


## Ring of disagrees

Performances on MaxCut on 2-regular graphs
-> Graph == ring

for $p <= n/2$, for each edge, subgraph == segment of size 2p+2

For each p, we only have subgraph line of 2p+2
qbits

We can increase the circuit depth by splitting the edge sum in C into 2 sums over $<j, j+1>$ (?)

### 3-regular graph

Shapes : isolated triangles or squares

better ratio if graph of size 4



## Relation to Quantum Adiabatic Algorithm

QAA exact solver, can be exponentially long.

## Variant
