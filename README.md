# Project and Master thesis


## Description

This repository aims to compile my work regarding towards both my Individual Project and Master thesis.

## Organization

The project folder contains code for experimentations regarding different optimization problems transformed into QUBO/QAOA form, along with a journal keeping track of experimentation, and the general progress.

The thesis folder contains the papers I'm currently reading along with some notes or reading reports
