Knapsack problem

#
- 0/1

Let k items of value $v_k$ and weight $w_k$

Let C the capacity of the bag

The binary variable $b_{i}$ represents the fact of putting an item in the bag

We maximize $\sum_{i = 1}^{k}{b_{i}v_{i}}$

Constrained by $\sum_{i = 1}^{k}{b_{i}w_{i}} <= C$

#

We introduce  $p = \log_{2}{n-1}$ binary called p_{j} variables to represent up to C, with n such as $C <= 2^{n} -1$. Each binary variable tied to $r_{j \in [0, n - 1[} = 2^{j}$ and $r_{j-1} = C - \sum_{j = 0}^{n-2}{2^{j}}$

Their values are set to 0 so that they don't impact the value.

which gives us a penalty $P((\sum_{i=1}^{k}{b_{i} * w_{i}} + \sum_{j = 1}^{n}{p_{j}  r_{j}} - C)^{2})$


The value of the penalty $P = \sum_{i = 1}^{k}{|v_{i}|}$.

The objective becomes $max \sum_{i = 1}^{k}{b_{i}v_{i}} - P((\sum_{i=1}^{k}{b_{i} * w_{i}} + \sum_{j = 1}^{n}{p_{j} r_{j} } - C)^{2})$

# Results:

On tests runs, the lowest(s) energy levels are correct answers and the absolute value of the energy represent the maximized value

# Bounded / unconstrained

When several items can be taken, my instinct is to use binary variables to represent each possible item, but this doesn't seem very scalable, and doesn't come near to work on unconstrained knapsack (we'd have to enumerate up to $C/w_{i}$ variables )

# QAOA

The expanded form of the problem is:
$$max \sum_{i = 1}^{k}{b_{i} (v_{i} - P w_{i}^{2} + 2 P C w_{i})}$$
$$+ \sum_{j = 1}^{n}{p_{i} (- P r_{j}^{2} + 2 P C r_{j})}$$
$$+ \sum_{i = 1}^{k - 1}{\sum_{l = i + 1}^{k}{b_{i} b_{l} (-2 P w_{i} w_{l})}}$$
$$+ \sum_{j = 1}^{n - 1}{\sum_{m = j + 1}^{n}{p_{j} p_{m} (-2 P r_{j} r_{m})}}$$
$$+ \sum_{i = 1}^{k}{\sum_{j = 1}^{n}{b_{i} p_{j} (-2 P w_{i} r_{j})}} - P C^{2}$$


# QAOA : gates

By assigning the $b_{i}$ to the first k qbits and $p_{i}$ to the last n qbits, naming those $k + n$ qbits $Q_{i}$, considering $r_{j}$ as their $w_{i}$ and assigning 0 for their $v_{j}$.
Using the relation between s and x, we have gates:

$$RZ(2 (v_{i}* (- P w_{i}^{2})  *( 2 P C w_{i}), i \in [1,k])$$
$$RZ(2 (- P r_{j}^{2} + 2 P C r_{j}), j \in [1, n])$$
$$RZZ(-4 P w_{i} w_{l} , i \in [1, k-1], l \in [i+1 , k])$$
$$RZZ(-4 P r_{j} r_{m}, j \in [1, n-1], l \in [j+1, n])$$
$$RZZ(-4 P w_{i} r_{j}, i \in [1, k] , j \in [1, n])$$

I didn't find where to add the constant $C^{2}$, and the results appeared random,
It might also be a problem in the optimisation process, as I


# QAOA


$$\sum_{i = 1}^{k+n}{Q_{i} V_{i} - P W_{i}^{2} + 2 P C W_{i}}$$
$$+ \sum_{i = 1}^{k+n-1}{\sum_{j>i}^{k+n}{-2 P W_{i} Q_{i}}}$$


# Questions

- In QAOA, we optimize the frequency of fiding the best result ?
- Initialization of the $\gamma$ and $\beta$ ?
- Are my problems coming from wrong gates, from an ordering in the application of the gates, from failing to optimize the right parameter, or from bad meta-paramaters?
