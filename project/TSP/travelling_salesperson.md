Let G a undirected weighted graph, N its nodes and E its edges

$w_{e}$ is the weight associated with edges $e \in E$

The graph can be made into a complete one by adding edges with a significantly high weight.

The objective is to find an Hamiltonian cycle P with the minimum weight.

We number the nodes and consider the objective as finding the best permutation.


We write $i$ the position in the order and $j$ the numbered node
We consider n+1 position, where the (n+1)th position represent the connection to the starting node.

We assign to each pair of i and j a binary variable $x_{ij}$ which takes the value 1 if the value $j$ is at position $i$ in the order, 0 otherwise.

We apply constraints that for each position $i$, there is only one node $j$ for $i$ from $1$ to $n$. We leave the cycle constraint for later.

$\sum_{j=1}^{n}{x_{ij}} = 1 \forall 1 \leq i \leq n$

and the resulting penalty $P(1 - \sum_{j=1}^{n}{x_{i,j}})^{2}$ for each $i$
which gives:

$P \sum_{i=1}^{n}{(1 - \sum_{j=1}^{n}{x_{i,j}})^{2}}$

and the constraint that for each node $j$ there is a unique position $i$

$\sum_{i=1}^{n}{x_{ij}} = 1 \forall 1 \leq j \leq n$

and penalty

$P \sum_{j=1}^{n}{(1 - \sum_{i=1}^{n}{x_{ij}})^{2}}$


We write an edge as $e_{j,k} \forall j \in n, k \in n, j \neq k$, and the corresponding weight $w_{j,k}$.

We have an objective function

$$min \sum_{i = 1}^{n}{\sum_{j = 1}^{n}{\sum_{k= 1, k\neq j}^{n} {x_{i,j}  x_{(i \mod n) + 1, k} w_{j,k}}}}$$


$$+ P\sum_{i=1}^{n}{(1 - \sum_{j=1}^{n}{x_{ij}})^{2}}$$
$$+ P\sum_{j=1}^{n}{(1 - \sum_{i=1}^{n}{x_{ij}})^{2}}$$

with $P = \sum_{j,k}{w_{jk}}$

### We expand the squared constraints :

$$min \sum_{i = 1}^{n}{\sum_{j = 1}^{n}{\sum_{k= 1, k\neq j}^{n} {x_{i,j}  x_{(i \mod n)+1, k} w_{j,k}}}}$$

$$+ 2P\sum_{i=1}^{n}{(\sum_{j=1}^{n}{-x_{ij}})}$$
$$+ P\sum_{i=1}{n}{(\sum_{j=1}^{n}{x_{ij}})^{2}}$$
$$+ P\sum_{j=1}^{n}{(\sum_{i=1}^{n}{-x_{ij}})}$$
$$+ P\sum_{j=1}{n}{(\sum_{i=1}^{n}{x_{ij}})^{2}}$$
$$+ 2Pn$$

###

$$min \sum_{i = 1}^{n}{\sum_{j = 1}^{n}{\sum_{k= 1, k\neq j}^{n} {x_{i,j}  x_{(i \mod n)+1, k} w_{j,k}}}}$$

$$+ 2P\sum_{i=1}^{n}{(\sum_{j=1}^{n}{-x_{ij}})}$$
$$+ 2Pn$$

$$+ 2P\sum_{i=1}{n}{\sum_{j=1}^{n}{\sum_{k > j}^{n}{x_{ij}x_{ik}}}}$$
$$+ 2P\sum_{j=1}{n}{\sum_{i=1}^{n}{\sum_{l > i}^{n}{x_{ij}x_{lj}}}}$$




